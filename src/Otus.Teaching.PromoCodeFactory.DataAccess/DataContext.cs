﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public class DataContext : DbContext
{
    
    public DbSet<Customer> Customers { get; set; } = null!;
    public DbSet<Preference> Preferences { get; set; } = null!;
    public DbSet<PromoCode> PromoCodes { get; set; } = null!;
    public DbSet<Employee> Employees { get; set; } = null!;
    public DbSet<Role> Roles { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // connect to sqlite database
        options.UseSqlite("Data Source=efCore_test_db.db");
    }
}